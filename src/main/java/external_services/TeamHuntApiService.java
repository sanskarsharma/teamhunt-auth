package external_services;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

public class TeamHuntApiService {

    /*
    *
    This class is a service abstraction for internally calling the query service
    Its methods just call the appropriate api endpoint on the query service
    *
    */

    private static String getServiceUrl(){
        return MessageFormat.format("http://{0}:{1}",
            System.getenv("TEAMHUNT_SERVICE_HOST"),
            System.getenv("TEAMHUNT_SERVICE_PORT")
        );
    }

    private Map getPlayerByName(String name) {

        String apiUrl = getServiceUrl() + "/players/" + name ;

        // call query service uing any HttpUtils lib

        return new HashMap();

    }

    private Map getPlayerByTeamName(String teamName) {

        String apiUrl = getServiceUrl() + "/players/team" + teamName ;

        // call query service uing any HttpUtils lib

        return new HashMap();

    }

}
