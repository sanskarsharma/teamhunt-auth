package client_app;

import client_app.ClientApp;
import client_app.ClientAppRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientAppService {

    @Autowired
    private ClientAppRepository clientAppRepository;

    public ClientApp getByApiKey(String apiKey) {
        return clientAppRepository.findByApiKey(apiKey);
    }
}
