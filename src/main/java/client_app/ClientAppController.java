package client_app;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientAppController {

    /*
    *
    This controller has endpoints to support endpoints to
    create, update or delete client apps i.e api-key related crud operations
    *
    */

    @Autowired
    private ClientAppService clientAppService;

}
