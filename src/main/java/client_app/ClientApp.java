package client_app;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="client_apps")
public class ClientApp {

    @Id
    private String id;

    @Field("api_key")
    private String apiKey;
    private String email;
    private String organization;

    public ClientApp() {}

    @Override
    public String toString() {
        return String.format(
                "ClientApp [id=%s, api_key='%s']",
                id, apiKey
        );
    }
}
