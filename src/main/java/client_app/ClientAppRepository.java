package client_app;

import client_app.ClientApp;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ClientAppRepository extends MongoRepository<ClientApp, String> {

    ClientApp findByApiKey(String apiKey);

}



