package api;

import client_app.ClientApp;
import client_app.ClientAppService;
import external_services.TeamHuntApiService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class MasterController {

    /*
    This controller class has all the public endpoints exposed by the application
    It also handles auth i.e verifies that incoming request contains api key.
    */

    @Autowired
    private ClientAppService clientAppService;

    @Autowired
    private TeamHuntApiService teamHuntApiService;

    @RequestMapping("/players")
    public ResponseEntity getAllPlayers(@RequestParam Map<String, String> queryParams) {

        if (queryParams.containsKey("api_key")) {

            ClientApp clientApp = clientAppService.getByApiKey(queryParams.get("api_key"));

            // call appropriate method of TeamHuntApiService and return results as hashmap

            // return Collections.singletonMap("response", "Hello World");

            return new ResponseEntity(HttpStatus.OK);

        }

        return new ResponseEntity(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED);

    }


}
